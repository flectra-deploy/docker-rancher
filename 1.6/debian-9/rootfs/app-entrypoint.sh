#!/bin/bash -e

. /opt/flectrahq/base/functions
. /opt/flectrahq/base/helpers

print_welcome_page

if [[ "$1" == "nami" && "$2" == "start" ]] || [[ "$1" == "/init.sh" ]]; then
    nami_initialize flectra
    info "Starting flectra... "
fi

exec tini -- "$@"
