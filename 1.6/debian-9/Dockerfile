#Partially from Odoo's dockerfile
#github.com/odoo/docker

FROM debian:stretch
MAINTAINER FlectraHQ <flectra@flectrahq.com>

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8
ENV FLECTRA_VERSION=1.6
ENV FLECTRA_RELEASE=latest

# Install some deps, lessc and less-plugin-clean-css, and wkhtmltopdf
RUN set -x; \
        apt-get update \
        && apt-get install -y --no-install-recommends \
            ca-certificates \
            curl \
            node-less \
            python3-pip \
            python3-setuptools \
            python3-renderpm \
            libssl1.0-dev \
            xz-utils \
            python3-watchdog \
        && curl -o wkhtmltox.tar.xz -SL https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz \
        && echo '3f923f425d345940089e44c1466f6408b9619562 wkhtmltox.tar.xz' | sha1sum -c - \
        && tar xvf wkhtmltox.tar.xz \
        && cp wkhtmltox/lib/* /usr/local/lib/ \
        && cp wkhtmltox/bin/* /usr/local/bin/ \
        && cp -r wkhtmltox/share/man/man1 /usr/local/share/man/

# Install Flectra
RUN set -x; \
        curl -o flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_all.deb -SL https://download.flectrahq.com/1.0/pub/deb/flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_all.deb \
		&& curl -o flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_amd64.changes -SL https://download.flectrahq.com/1.0/pub/deb/flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_amd64.changes \
		&& echo "`sed -n '/^Checksums-Sha1:$/{n;p;n;p;n;p}' flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_amd64.changes | tail -1 | awk '{print $1}'` flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_all.deb" | sha1sum -c \
        && dpkg --force-depends -i flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_all.deb \
        && apt-get update \
        && apt-get -y install -f --no-install-recommends \
        && rm -rf /var/lib/apt/lists/* flectra_${FLECTRA_VERSION}.${FLECTRA_RELEASE}_all.deb

# Copy entrypoint script and Odoo configuration file
RUN pip3 install num2words xlwt
COPY ./app-entrypoint.sh /
COPY ./flectra.conf /etc/flectra/
RUN chown flectra /etc/flectra/flectra.conf
RUN chmod +x /app-entrypoint.sh

# Mount /var/lib/flectra to allow restoring filestore and /mnt/extra-addons for users addons
RUN mkdir -p /mnt/extra-addons \
        && chown -R flectra /mnt/extra-addons
VOLUME ["/var/lib/flectra", "/mnt/extra-addons"]

#################
# Install required system packages and dependencies
RUN install_packages ghostscript imagemagick libbsd0 libbz2-1.0 libc6 libedit2 libffi6 libgcc1 libgcrypt20 libgmp10 libgnutls30 libgpg-error0 libhogweed4 libicu57 libidn11 libldap-2.4-2 liblzma5 libncurses5 libnettle6 libp11-kit0 libpq5 libreadline7 libsasl2-2 libsqlite3-0 libssl1.1 libstdc++6 libtasn1-6 libtinfo5 libuuid1 libxml2 libxslt1.1 zlib1g
RUN bitnami-pkg install python-3.6.9-3 --checksum c4d4542cde9d0c0e5d0239491cdd91cacc68319679d9fab4088a994c9d40a7f6
RUN bitnami-pkg install postgresql-client-11.6.0-0 --checksum 84762a54c89698bd4bf9bc380252591492b67464918c291cf2b666e1a74b8f3b
RUN bitnami-pkg install node-7.10.1-1 --checksum 53362288e961d298a724353710a31901d4cb8237894a26a2f676b35450c02975

COPY rootfs /
ENV APP_NAME="flectra" \
    IMAGE_VERSION="1.6.3" \
    FLECTRA_EMAIL="user@example.com" \
    FLECTRA_PASSWORD="flectra" \
    PATH="/opt/flectrahq/python/bin:/opt/flectrahq/postgresql/bin:/opt/flectrahq/node/bin:$PATH" \
    POSTGRESQL_HOST="postgresql" \
    POSTGRESQL_PASSWORD="" \
    POSTGRESQL_PORT_NUMBER="5432" \
    POSTGRESQL_USER="postgres" \
    SMTP_HOST="" \
    SMTP_PASSWORD="" \
    SMTP_PORT="" \
    SMTP_PROTOCOL="" \
    SMTP_USER=""
#################

# Expose Flectra services
EXPOSE 7073 7072

# Set the default config file
ENV FLECTRA_RC /etc/flectra/flectra.conf

# Set default user when running the container
USER flectra

ENTRYPOINT ["/app-entrypoint.sh"]
CMD ["flectra"]